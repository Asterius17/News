//
//  Repository.swift
//  News
//
//  Created by chameleon on 11.07.17.
//  Copyright © 2017 chameleon. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift
import Swinject

protocol PRepository {
    func getInfo()
    
}

class Message: Object {
    dynamic var text = ""
}

class CRepository: PRepository{
    func getInfo() {
        let message = Message()
        // Request to the server
        Alamofire.request("https://httpbin.org/get").responseJSON { response in
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            if let json = response.result.value {
                print("JSON: \(json)") // serialized json response
            }
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)") // original server data as UTF8 string
            }
        }
    }

}
